package game.nerd.duel;

import game.nerd.duel.players.Player;

public class Shot {
	Tile target;
	Player shooter;
	int shotPower;
	int tilesTraveled;
	
	public Shot(Tile target, Player shooter)
	{
		this.target = target;
		this.shooter = shooter;
		tilesTraveled = 0;
		shotPower = shooter.getFirepower();
	}

	public Tile getTarget() {
		return target;
	}

	public Player getShooter() {
		return shooter;
	}

	public int getShotPower() {
		return shotPower;
	}

	public int getTilesTraveled() {
		return tilesTraveled;
	}
	
	public void travelledTile()
	{
		tilesTraveled = tilesTraveled + 1;
	}
}
