package game.nerd.duel.players;

import java.util.List;

import game.nerd.Util;
import game.nerd.duel.Tile;
import game.nerd.duel.VoTile;

public class PlayerOne extends Player {

	public PlayerOne(int id, int bonus) {
		super(id, bonus);
	}

	@Override
	public String getColour() {
		return "222299";
	}

	protected void setUp() {
		this.addHealth(10);
		this.addSightRange(9);
		this.addArmour(2);
		this.addFirepower(6);
		this.addGenerator(16);
		this.addFireRange(6);
	}

	@Override
	protected void bonusReceived() {
		int bonus = getBonus();

		if (getHealth() < 10) {
			addHealth(bonus);
		}

		if (getFireRange() < getSightRange()) {
			addFireRange(bonus);
		}

		if (getSightRange() < 10) {
			addSightRange(bonus);
		}

		if (bonus >= BONUS_PER_MOVE_SPEED) {
			addMoveSpeed(bonus);
		}
	}

	@Override
	protected void playerMove() {

		while (powerPerMove() + POWER_PER_LOOK_ACTION < getPower()) {
			VoTile[][] lookAtMap = look();
			VoTile enemyTile = tryToEngage(lookAtMap);

			if (enemyTile == null) {
				moveTowardsBonus(lookAtMap);
			}
		}
	}

	private void moveTowardsBonus(VoTile[][] lookAtMap) {
		VoTile towardsHasBonusTile = towardsHasBonusTile(lookAtMap);

		if (towardsHasBonusTile != null) {
			movePlayer(towardsHasBonusTile);
		} else {
			VoTile adjacentRandomNonBlockerTile = adjacentRandomNonBlockerTile(lookAtMap);

			if (adjacentRandomNonBlockerTile != null) {
				movePlayer(adjacentRandomNonBlockerTile);
			}
		}
	}

	private VoTile tryToEngage(VoTile[][] lookAtMap) {
		List<VoTile> enemyTileList = findEnemyTiles(lookAtMap);
		VoTile enemyVoTile = null;

		if (!enemyTileList.isEmpty()) {
			enemyVoTile = Util.getRandomElement(enemyTileList);
			Tile tile = getTile();
			int distanceToEnemy = Util.distance(tile.getColumn(),tile.getRow(),enemyVoTile.getMapColumn(),enemyVoTile.getMapRow());
			
			if (distanceToEnemy <= getFireRange() && getShotPowerAtDistance(getFirepower(),distanceToEnemy) > 0  && clearShot(enemyVoTile)) {
				fire(enemyVoTile);
			} else {
				VoTile stepToEnemyTile = legalStep(lookAtMap, getPlayerVoTile(lookAtMap), enemyVoTile,
						VoTile::hasBlocker);
				movePlayer(stepToEnemyTile);
			}
		}
		return enemyVoTile;
	}

}
