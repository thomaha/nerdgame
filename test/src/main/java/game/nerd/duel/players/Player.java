package game.nerd.duel.players;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import game.nerd.Util;
import game.nerd.duel.MapPane;
import game.nerd.duel.Shot;
import game.nerd.duel.Tile;
import game.nerd.duel.VoTile;
import javafx.scene.Node;

public abstract class Player extends Node {

	// Players attributes
	private int sightRange; // How far player can see
	private int health; // How many health points player has
	private int fireRange; // How far player can fire
	private int firepower; // How much damage player does at range 1
	private int moveSpeed; // How much power player use per step
	private int bonus; // How much bonus the player has to spend
	private int generator; // How much power player generates per turn.
	private int armour; // How much armour player has to reduce damage
	private int power; // Current power
	private int score; // Players score
	private int playerId;
	private int shotsFired; // How many shots the player has fired
	private int moves; // How many moves this player has had

	// Context
	private MapPane mapPane;
	private Tile tile;

	// Conversion factors
	public static final int BONUS_PER_SIGHT_RANGE = 3;
	public static final int BONUS_PER_GENERATOR = 3;
	public static final int BONUS_PER_FIRE_RANGE = 2;
	public static final int BONUS_PER_FIRE_POWER = 2;
	public static final int BONUS_PER_MOVE_SPEED = 5;
	public static final int BONUS_PER_HEALTH = 1;
	public static final int BONUS_PER_ARMOUR = 1;

	public static final int POWER_PER_TURN = 10;
	public static final int POWER_PER_LOOK_ACTION = 2;
	public static final int POWER_PER_FIRE_ACTION = 5;
	public static final int POWER_PER_MOVE_ACTION = 10;
	private static final double SHOT_POWER_REDUCTION_PER_TILE = 0.90;

	public Player(int playerId, int bonus) {
		this.playerId = playerId;
		this.bonus = bonus;
		setUp();
	}

	public void setMapPane(MapPane mapPane) {
		this.mapPane = mapPane;
	}

	/**
	 * Called when player get to move. Adds power and let sub class handle the
	 * action.
	 */
	public void move() {
		power = power + powerPerTurn();		
		playerMove();
	}

	protected int powerPerTurn() {
		float generatorFactor = ((float) generator / POWER_PER_TURN);
		return (int) ((1 + generatorFactor) * POWER_PER_TURN);
	}

	protected int powerPerMove() {
		float speedFactor = ((float) 9 / (10 + moveSpeed));
		return (int) (POWER_PER_MOVE_ACTION * speedFactor);
	}

	protected int damagePerShot(int shotPowerLeft) {
		float armourFactor = ((float) 5 / (5 + armour));
		return (int) (shotPowerLeft * armourFactor);
	}

	/**
	 * @return Shallow copy of the part of the map the player can see. Subtracts
	 *         power for looking.
	 */
	protected VoTile[][] look() {
		if (power >= POWER_PER_LOOK_ACTION) {
			power = power - POWER_PER_LOOK_ACTION;
			return mapPane.look(this);
		} else {
			return new VoTile[0][0];
		}
	}

	/**
	 * Fire at the target tile. Subtracts power for shooting.
	 * 
	 * @param target
	 */
	protected void fire(VoTile target) {
		if (power >= POWER_PER_FIRE_ACTION) {
			power = power - POWER_PER_FIRE_ACTION;
			Tile targetTile = mapPane.getTile(target.getMapColumn(), target.getMapRow());
			Shot shot = new Shot(targetTile, this);
			shotsFired = shotsFired + 1;
			mapPane.fire(shot);
			moves = moves + 1;
		}
	}

	/**
	 * Is called when the tile holding this player is hit by a shot. Subtracts
	 * health as per the power of the shot and the players armour. Shot power is
	 * reduced per tile travelled.
	 * 
	 * @param shot
	 * @return
	 */
	public int isHit(Shot shot) {
		int shotPowerLeft = getShotPowerAtDistance(shot.getShotPower(), shot.getTilesTraveled());
		int damage = damagePerShot(shotPowerLeft);
		health = health - damage;
		return damage;
	}

	public int getShotPowerAtDistance(int shotPower, int distance) {
		return (int) (shotPower * Math.pow(SHOT_POWER_REDUCTION_PER_TILE, distance));
	}

	/**
	 * Moves the player if target tile is legal. Subtracts power for the move if
	 * move is performed.
	 * 
	 * @param toTile
	 * @return True if move was accepted
	 */
	protected boolean movePlayer(VoTile voToTile) {
		boolean retVal = false;		
		
		if (powerPerMove() < getPower()) {			
			Tile toTile = mapPane.getTile(voToTile.getMapColumn(), voToTile.getMapRow());

			// Need to verify tile data as VoTile might be stale if player has not looked
			// recently
			if (toTile != null && !toTile.hasBlocker() && !toTile.hasPlayer()) {
				tile.setPlayer(null);
				toTile.setPlayer(this);
				tile = toTile;
				power = power - powerPerMove();
				moves = moves + 1;
				
				if (tile.hasBonus()) {
					score = score + 1;
					bonus = bonus + 1;
					tile.setBonus(false);
					bonusReceived();
				}
				retVal = true;
			}
		}
		return retVal;
	}

	public int getPlayerId() {
		return playerId;
	}

	public int getSightRange() {
		return sightRange;
	}

	public void setSightRange(int sightRange) {
		this.sightRange = sightRange;
	}

	public int getHealth() {
		return health;
	}

	public boolean isAlive() {
		return health >= 0;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getFireRange() {
		return fireRange;
	}

	public void setFireRange(int fireRange) {
		this.fireRange = fireRange;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public abstract String getColour();

	public Tile getTile() {
		return tile;
	}

	public void setTile(Tile tile) {
		this.tile = tile;
	}

	public int getScore() {
		return score;
	}

	public int getMoves() {
		return moves;
	}

	/**
	 * Convert max useBonus into sight range. Bonus used is deducted from players
	 * bonus.
	 * 
	 * @param useBonus
	 * @return Points added to sight range
	 */
	protected int addSightRange(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_SIGHT_RANGE) {
			added = useBonus / BONUS_PER_SIGHT_RANGE;
			sightRange = sightRange + added;
			bonus = bonus - (added * BONUS_PER_SIGHT_RANGE);
		} else {
			added = 0;
		}
		return added;
	}

	/**
	 * Convert max useBonus into fire range. Bonus used is deducted from players
	 * bonus.
	 * 
	 * @param useBonus
	 * @return Points added to fire range
	 */
	protected int addFireRange(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_FIRE_RANGE) {
			added = useBonus / BONUS_PER_FIRE_RANGE;
			fireRange = fireRange + added;
			bonus = bonus - (added * BONUS_PER_FIRE_RANGE);
		} else {
			added = 0;
		}
		return added;
	}

	/**
	 * Convert max useBonus into fire power. Bonus used is deducted from players
	 * bonus.
	 * 
	 * @param useBonus
	 * @return Points added to fire power
	 */
	protected int addFirepower(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_FIRE_POWER) {
			added = useBonus / BONUS_PER_FIRE_POWER;
			firepower = firepower + added;
			bonus = bonus - (added * BONUS_PER_FIRE_POWER);
		} else {
			added = 0;
		}
		return added;
	}

	/**
	 * Convert max useBonus into generator. Bonus used is deducted from players
	 * bonus.
	 * 
	 * @param useBonus
	 * @return Points added to generator
	 */
	protected int addGenerator(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_GENERATOR) {
			added = useBonus / BONUS_PER_SIGHT_RANGE;
			generator = generator + added;
			bonus = bonus - (added * BONUS_PER_GENERATOR);
		} else {
			added = 0;
		}
		return added;
	}

	/**
	 * Convert max useBonus into health. Bonus used is deducted from players bonus.
	 * 
	 * @param useBonus
	 * @return Points added to health
	 */
	protected int addHealth(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_HEALTH) {
			added = useBonus / BONUS_PER_HEALTH;
			health = health + added;
			bonus = bonus - (added * BONUS_PER_HEALTH);
		} else {
			added = 0;
		}
		return added;
	}

	/**
	 * Convert max useBonus into armour. Bonus used is deducted from players bonus.
	 * 
	 * @param useBonus
	 * @return Points added to armour
	 */
	protected int addArmour(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_ARMOUR) {
			added = useBonus / BONUS_PER_ARMOUR;
			armour = armour + added;
			bonus = bonus - (added * BONUS_PER_ARMOUR);
		} else {
			added = 0;
		}
		return added;
	}

	/**
	 * Convert max useBonus into move speed. Bonus used is deducted from players
	 * bonus.
	 * 
	 * @param useBonus
	 * @return Points added to armour
	 */
	protected int addMoveSpeed(int useBonus) {
		int added;

		if (bonus >= useBonus && bonus >= BONUS_PER_MOVE_SPEED) {
			added = useBonus / BONUS_PER_MOVE_SPEED;
			moveSpeed = moveSpeed + added;
			bonus = bonus - (added * BONUS_PER_MOVE_SPEED);
		} else {
			added = 0;
		}
		return added;
	}

	public int getFirepower() {
		return firepower;
	}

	public int getMoveSpeed() {
		return moveSpeed;
	}

	public int getGenerator() {
		return generator;
	}

	public int getArmour() {
		return armour;
	}

	public int getBonus() {
		return bonus;
	}

	public int getShotsFired() {
		return shotsFired;
	}

	/**
	 * Methods that must be implemented by the player implementations
	 */

	/**
	 * Distribute the initial bonus on the attributes
	 */
	protected abstract void setUp();

	/**
	 * Called when player gets a bonus so that player can spend it.
	 */
	protected abstract void bonusReceived();

	/**
	 * Called when the player gets to move. Player can do stuff as long as player
	 * has power to do it.
	 */
	protected abstract void playerMove();

	/**
	 * Movement related
	 */

	/**
	 * Try to find a bonus tile within sight range and move one step towards that
	 * 
	 * @param lookAtMap Part of map sightRange from player
	 * @return Tile towards a bonus tile or null if none found within sight range
	 */
	public VoTile towardsHasBonusTile(VoTile[][] lookAtMap) {
		// Find the tile the player is at. Not centre if player is closer to map edge
		// than sight range
		VoTile playerVoTile = getPlayerVoTile(lookAtMap);
		VoTile retVal = null;

		if (playerVoTile != null) {
			for (int i = 1; i <= sightRange && retVal == null; i++) {
				retVal = findTile(lookAtMap, playerVoTile, VoTile::hasBonus, i);
				retVal = legalStep(lookAtMap, playerVoTile, retVal, VoTile::hasBlocker);
			}
		}
		return retVal;
	}

	protected VoTile getPlayerVoTile(VoTile[][] lookAtMap) {
		VoTile retVal = null;

		for (int i = 0; i < lookAtMap.length; i++) {
			for (int j = 0; j < lookAtMap[i].length; j++) {
				VoTile voTile = lookAtMap[i][j];
				if (voTile != null && voTile.getPlayerId() != 0 && voTile.getPlayerId() == playerId) {
					retVal = voTile;
				}
			}
		}
		return retVal;
	}

	/**
	 * Find a tile i steps from current tile
	 * 
	 * @param Tile[][]   lookAtMap
	 * @param tileColumn of starting tile
	 * @param tileRow    of starting tile
	 * @param predicate  Test to perform on target tile
	 * @param i          distance from starting tile
	 * @return
	 */
	private VoTile findTile(VoTile[][] lookAtMap, VoTile playerVoTile, Predicate<VoTile> predicate, int i) {
		List<VoTile> foundTiles = new ArrayList<>();
		VoTile voTile;
		int tileColumn = playerVoTile.getLookColumn();
		int tileRow = playerVoTile.getLookRow();

		for (int j = -i; j <= i; j++) {
			voTile = checkTile(lookAtMap, tileColumn + j, tileRow + i, predicate);

			if (tile != null) {
				foundTiles.add(voTile);
			}
		}

		voTile = checkTile(lookAtMap, tileColumn + i, tileRow, predicate);

		if (voTile != null) {
			foundTiles.add(voTile);
		}

		voTile = checkTile(lookAtMap, tileColumn - i, tileRow, predicate);

		if (voTile != null) {
			foundTiles.add(voTile);
		}

		for (int j = -i; j <= i; j++) {
			voTile = checkTile(lookAtMap, tileColumn + j, tileRow - i, predicate);

			if (voTile != null) {
				foundTiles.add(voTile);
			}
		}

		// Select a random tile from the tiles found
		if (!foundTiles.isEmpty()) {
			voTile = Util.getRandomElement(foundTiles);
		}
		return voTile;
	}

	protected List<VoTile> findTiles(VoTile[][] lookAtMap, Predicate<VoTile> predicate) {
		List<VoTile> foundTiles = new ArrayList<>();

		for (int i = 0; i <= lookAtMap.length; i++) {
			for (int j = 0; j <= lookAtMap[i].length; j++) {
				VoTile voTile = checkTile(lookAtMap, i, j, predicate);

				if (tile != null) {
					foundTiles.add(voTile);
				}
			}
		}
		return foundTiles;
	}

	protected List<VoTile> findEnemyTiles(VoTile[][] lookAtMap) {
		List<VoTile> foundTiles = new ArrayList<>();

		for (int i = 0; i < lookAtMap.length; i++) {
			for (int j = 0; j < lookAtMap[i].length; j++) {

				VoTile voTile = checkTile(lookAtMap, i, j, VoTile::hasPlayer);

				// If a tile with a player is found, skip if it is this players tile
				if (tile != null && voTile != null
						&& (voTile.getMapColumn() != tile.getColumn() || voTile.getMapRow() != tile.getRow())) {
					foundTiles.add(voTile);
				}
			}
		}
		return foundTiles;
	}

	private VoTile checkTile(VoTile[][] lookAtMap, int column, int row, Predicate<VoTile> predicate) {
		VoTile adjacentTile = getTile(lookAtMap, column, row);
		return (adjacentTile != null && predicate.test(adjacentTile)) ? adjacentTile : null;
	}

	public VoTile adjacentRandomNonBlockerTile(VoTile[][] lookAtMap) {
		VoTile playerVoTile = getPlayerVoTile(lookAtMap);
		int tileColumn = playerVoTile.getLookColumn();
		int tileRow = playerVoTile.getLookRow();
		VoTile retVal = null;

		while (retVal == null) {
			int columnDelta = Util.randomMoveNumber();
			int rowDelta = Util.randomMoveNumber();

			if (columnDelta != 0 || rowDelta != 0) {
				retVal = negateCheckTile(lookAtMap, tileColumn + columnDelta, tileRow + rowDelta, VoTile::hasBlocker);
			}
		}
		return retVal;
	}

	private VoTile negateCheckTile(VoTile[][] lookAtMap, int column, int row, Predicate<VoTile> predicate) {
		VoTile adjacentTile = getTile(lookAtMap, column, row);
		return (adjacentTile != null && !predicate.test(adjacentTile)) ? adjacentTile : null;
	}

	/**
	 * @param fromTile
	 * @param toTile
	 * @param predicate
	 * @return A legal step on the path from fromTile to toTile
	 */
	public VoTile legalStep(VoTile[][] lookAtMap, VoTile fromTile, VoTile toTile, Predicate<VoTile> predicate) {
		VoTile retVal = null;

		if (fromTile != null && toTile != null) {
			int fromColumn = fromTile.getLookColumn();
			int fromRow = fromTile.getLookRow();
			int toColumn = toTile.getLookColumn();
			int toRow = toTile.getLookRow();

			int stepColumn = Util.stepFactor(fromColumn, toColumn);
			int stepRow = Util.stepFactor(fromRow, toRow);
			retVal = stepStraight(lookAtMap, fromColumn, stepColumn, fromRow, stepRow, predicate);

			// Attempt step sideways to pass obstacle
			if (retVal == null) {
				retVal = stepSideways(lookAtMap, stepColumn, stepRow, fromColumn, fromRow, predicate);
			}

			// Attempt step diagonal to pass obstacle
			if (retVal == null || predicate.test(retVal)) {
				retVal = stepDiagonal(lookAtMap, stepColumn, stepRow, fromColumn, fromRow);
			}

			// All failed, try reverse.
			if (retVal == null || predicate.test(retVal)) {
				stepColumn = -stepColumn;
				stepRow = -stepRow;
				retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);

				if (retVal != null && predicate.test(retVal)) {
					retVal = null; // No legal move found
				}
			}
		}
		return retVal;
	}

	private VoTile stepDiagonal(VoTile[][] lookAtMap, int stepColumn, int stepRow, int fromColumn, int fromRow) {
		VoTile retVal;

		if (stepColumn == 1 && stepRow == -1) {
			stepColumn = -1;
			retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);
		} else if (stepColumn == -1 && stepRow == -1) {
			stepColumn = 1;
			retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);
		} else if (stepRow == 1) {
			stepRow = -1;
			retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);
		} else {
			retVal = null;
		}
		return retVal;
	}

	private VoTile stepSideways(VoTile[][] lookAtMap, int stepColumn, int stepRow, int fromColumn, int fromRow,
			Predicate<VoTile> predicate) {
		VoTile retVal = null;

		if (stepColumn == 0) {
			stepColumn = 1;
			retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);

			if (retVal == null || predicate.test(retVal)) {
				stepColumn = -1;
				retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);
			}
		} else if (stepRow == 0) {
			stepRow = 1;
			retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);

			if (retVal == null || predicate.test(retVal)) {
				stepRow = -1;
				retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);
			}
		}
		return retVal;
	}

	private VoTile stepStraight(VoTile[][] lookAtMap, int fromColumn, int stepColumn, int fromRow, int stepRow,
			Predicate<VoTile> predicate) {
		VoTile retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow + stepRow);

		// If not legal value, try moving only one direction
		if (predicate.test(retVal)) {
			if (stepColumn != 0) {
				retVal = getTile(lookAtMap, fromColumn + stepColumn, fromRow);

				if (retVal != null && predicate.test(retVal)) {
					retVal = null;
				}
			}

			if (stepRow != 0) {
				retVal = getTile(lookAtMap, fromColumn, fromRow + stepRow);

				if (retVal != null && predicate.test(retVal)) {
					retVal = null;
				}
			}
		}
		return retVal;
	}

	/**
	 * @param lookAtMap
	 * @param column
	 * @param row
	 * @return The tile in position [column][row] in lookAtMap if it exists
	 */
	private VoTile getTile(VoTile[][] lookAtMap, int column, int row) {
		if (column > -1 && row > -1 && column < lookAtMap.length && row < lookAtMap[column].length) {
			return lookAtMap[column][row];
		}
		return null;
	}

	/**
	 * @param enemyVoTile
	 * @return True if a shot can be fired from the players tile at the enemyVoTile
	 */
	protected boolean clearShot(VoTile enemyVoTile) {
		return mapPane.shootAtTile(tile.getColumn(), tile.getRow(), enemyVoTile.getMapColumn(), enemyVoTile.getMapRow(),
				null) != null;
	}
}
