package game.nerd.duel.players;

import java.util.List;

import game.nerd.Util;
import game.nerd.duel.Tile;
import game.nerd.duel.VoTile;

public class PlayerTwo extends Player {

	public PlayerTwo(int id, int bonus) {
		super(id, bonus);
	}

	@Override
	public String getColour() {
		return "22ff88";
	}

	protected void setUp() {
		this.addHealth(5);
		this.addSightRange(12);
		this.addArmour(2);
		this.addFirepower(8);
		this.addGenerator(4);
		this.addMoveSpeed(6);
		this.addFireRange(12);
	}

	@Override
	protected void bonusReceived() {
		int bonus = getBonus();

		if (getHealth() < 10) {
			addHealth(bonus);
		}

		if (getSightRange() < 12) {
			addSightRange(bonus);
		}
		
		if (getFireRange() < getSightRange()) {
			addFireRange(bonus);
		}
		
		if (bonus >= BONUS_PER_GENERATOR) {
			addGenerator(bonus);
		}
	}

	@Override
	protected void playerMove() {

		while (powerPerMove()  + POWER_PER_LOOK_ACTION < getPower()) {
			VoTile[][] lookAtMap = look();
			VoTile enemyTile = tryToEngage(lookAtMap);

			if (enemyTile == null) {
				moveTowardsBonus(lookAtMap);
			}
		}
	}

	private void moveTowardsBonus(VoTile[][] lookAtMap) {
		VoTile towardsHasBonusTile = towardsHasBonusTile(lookAtMap);

		if (towardsHasBonusTile != null) {
			movePlayer(towardsHasBonusTile);
		} else {
			VoTile adjacentRandomNonBlockerTile = adjacentRandomNonBlockerTile(lookAtMap);

			if (adjacentRandomNonBlockerTile != null) {
				movePlayer(adjacentRandomNonBlockerTile);
			}
		}
	}

	private VoTile tryToEngage(VoTile[][] lookAtMap) {
		List<VoTile> enemyTileList = findEnemyTiles(lookAtMap);
		VoTile enemyVoTile = null;

		if (!enemyTileList.isEmpty()) {
			enemyVoTile = Util.getRandomElement(enemyTileList);
			Tile tile = getTile();

			if (Util.distance(tile.getColumn(), tile.getRow(), enemyVoTile.getMapColumn(),
					enemyVoTile.getMapRow()) <= getFireRange() && clearShot(enemyVoTile)) {
				fire(enemyVoTile);
			} else {
				VoTile stepToEnemyTile = legalStep(lookAtMap, getPlayerVoTile(lookAtMap), enemyVoTile,
						VoTile::hasBlocker);
				movePlayer(stepToEnemyTile);
			}
		}
		return enemyVoTile;
	}	
}
