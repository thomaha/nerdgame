package game.nerd.duel;

/**
 * Data representation of tile that can be used by players
 *
 */
public class VoTile {
	private int mapRow;
	private int mapColumn;
	private boolean blocker;
	private int playerId;
	private boolean bonus;
	private int lookColumn;
	private int lookRow;

	public VoTile(Tile tile, int lookColumn, int lookRow) {
		mapColumn = tile.getColumn();
		mapRow = tile.getRow();
		blocker = tile.hasBlocker();

		if (tile.getPlayer() != null) {
			playerId = tile.getPlayer().getPlayerId();
		}
		bonus = tile.hasBonus();
		this.lookColumn = lookColumn;
		this.lookRow = lookRow;
	}

	public int getMapRow() {
		return mapRow;
	}

	public int getMapColumn() {
		return mapColumn;
	}

	public int getLookRow() {
		return lookRow;
	}

	public int getLookColumn() {
		return lookColumn;
	}

	public int getPlayerId() {
		return playerId;
	}

	public boolean hasBlocker() {
		return blocker;
	}

	public boolean hasBonus() {
		return bonus;
	}

	public boolean hasPlayer() {
		return playerId != 0;
	}
}