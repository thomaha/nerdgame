package game.nerd.duel;

import game.nerd.duel.players.Player;
import javafx.animation.FillTransition;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Tile extends Rectangle {

	private static final String BLOCKER_COLOUR = "662222";
	private static final String WHITE = "ffffff";
	private static final String BONUS_COLOUR = "66cccc";
	private int row;
	private int column;
	private boolean blocker = false;
	private Player player = null;
	private boolean bonus = false;

	private FillTransition fillTransition = new FillTransition(Duration.seconds(4));

	public Tile(int column, int row) {
		this.column = column;
		this.row = row;
		setWidth(App.TILE_SIZE);
		setHeight(App.TILE_SIZE);
		fillTransition.setFromValue(Color.RED);
		fillTransition.setShape(this);
	}

	public void setBlocker(boolean b) {
		blocker = b;
	}

	public void setPlayer(Player b) {
		player = b;
	}

	public void setBonus(boolean b) {
		bonus = b;
	}

	public void paint() {
		String colour;

		if (blocker) {
			colour = BLOCKER_COLOUR;
		} else if (player != null) {
			colour = player.getColour();
		} else if (bonus) {
			colour = BONUS_COLOUR;
		} else {
			colour = WHITE;
		}

		fillTransition.setToValue(Color.valueOf(colour));
		Paint paint = Paint.valueOf(colour);
		this.setFill(paint);
	}

	public void strike() {
		fillTransition.playFromStart();
	}

	public boolean tileEquals(Tile tile) {
		return tile != null && tile.getColumn() == column && tile.getRow() == row;
	}

	public boolean hasPlayer() {
		return player != null;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean isEmpty() {
		return !(blocker || player != null || bonus);
	}

	public boolean hasBlocker() {
		return blocker;
	}

	public boolean hasBonus() {
		return bonus;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	@Override
	public String toString() {
		return "column " + column + " row " + row + " blocker " + blocker + " bonus " + bonus + " player " + player;
	}

}
