package game.nerd;

import java.util.List;

public class Util {
	/**
	 * @param from
	 * @param to
	 * @return Factor on a step from from to to
	 */
	public static int stepFactor(int from, int to) {
		int retVal;

		if (from < to) {
			retVal = +1;
		} else if (from > to) {
			retVal = -1;
		} else {
			retVal = 0;
		}
		return retVal;
	}

	/**
	 * @return Random number [-1,0,1]
	 */
	public static int randomMoveNumber() {
		int retVal = (int) ((10 * Math.random()) % 3);

		if (retVal == 2) {
			retVal = -1;
		}
		return retVal;
	}

	/**
	 * Select a random entry from the dataList
	 * 
	 * @param <T>
	 * @param dataList
	 * @return Random entry
	 */
	public static <T> T getRandomElement(List<T> dataList) {
		T retVal;

		if (!dataList.isEmpty()) {
			if (dataList.size() == 1) {
				retVal = dataList.get(0);
			} else {
				retVal = dataList.get((int) ((Math.random() * 100) % dataList.size()));
			}
		} else {
			retVal = null;
		}
		return retVal;
	}

	/**
	 * @param fromColumn
	 * @param fromRow
	 * @param toColumn
	 * @param toRow
	 * @return Distance from one point to the other
	 */
	public static int distance(int fromColumn, int fromRow, int toColumn, int toRow) {
		int columnDistance = toColumn - fromColumn;
		int rowDistance = toRow - fromRow;
		return Math.abs(columnDistance) + Math.abs(rowDistance);
	}
}
