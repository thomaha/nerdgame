module game.nerd.duel {
	requires javafx.controls;
	requires javafx.fxml;
	requires transitive javafx.graphics;
	requires javafx.base;

	opens game.nerd.duel to javafx.fxml;

	exports game.nerd.duel;
	exports game.nerd.duel.players;
}